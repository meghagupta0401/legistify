<?php
session_start();
?>
<?php
include ("mysql.php") ;

if(isset($_SESSION['loginId']))
{
	if($_GET['type']=='user'){
		$_SESSION['type']='user';
			header("Location:userProfile.php");
	}
		

if($_GET['type']=='lawyer'){
	$_SESSION['type']='user';
	header("Location:lawyerProfile.php");
}
	
}
if(isset($_POST['logIn']))
{
	$type=$_GET['type'];
	$_SESSION['type']=$type;
	$result=mysqli_query($con,"select * from login where username='$_POST[username]' and password='$_POST[password]'");
	if(mysqli_num_rows($result)==1)
	{
		while($row=mysqli_fetch_array($result))
		{
			$_SESSION['loginId']=$row{'username'};
		//	echo $_SESSION['loginId'];
			
		}
		if($type=='user')
		header('Location:userProfile.php');
	if($type=='lawyer')
		
		header('Location:lawyerProfile.php');
	// echo "error";
		
	}
	else
	{
		echo "Invalid email or password";
	}
}


if(isset($_POST['signUp']))
	{
		if($_GET['type']=="user"){
		$type="user";
		 $insert_user="insert into user(name,phone_no,email,address,username)
		Values('$_POST[name]','$_POST[phone_no]','$_POST[email]','$_POST[address]','$_POST[username]')";
				}
				else if($_GET['type']=="lawyer"){
					$type="lawyer";
		 $insert_user="insert into lawyer(name,phone_no,email,address,username,profile_summary)
		Values('$_POST[name]','$_POST[phone_no]','$_POST[email]','$_POST[address]','$_POST[username]','$_POST[summary]')";
				}
				else{
					echo "no type";
				}
		
		
		if(!mysqli_query($con,$insert_user) )
		{
			
			die("error is there unable to signUp".mysqli_connect_errno()); 
		}
		else{
					
		   $insert_log="insert into login(username,password,type)
		  Values('$_POST[username]','$_POST[password]','$type')";
		  echo $insert_log ;
		  if(!mysqli_query($con,$insert_log)){
			die("error is there..fill the details properly".mysqli_connect_errno()); 
		}
		else{
			echo "Registered successfully";
		    header("Location: index.php");
		}
		}
		
			
		}
	
	
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Legistify </title>

  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link href="css/icheck/flat/green.css" rel="stylesheet">


  <script src="js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">
        <section class="login_content">
          <form name="form1" method="post">
            <h1>Login Form</h1>
            <div>
              <input type="text" class="form-control" placeholder="Username" name="username" required="" />
            </div>
            <div>
              <input type="password" class="form-control" placeholder="Password" name="password" required="" />
            </div>
            <div >
			
              <input type="submit" class="btn btn-default submit" name="logIn" value="Log in"/>
            
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <p class="change_link">New to site?
               
              </p>
			  <h2> <a href="#toregister" class="to_register"> Create Account </a></h2>
              <div class="clearfix"></div>
              <br />
              
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
      <div id="register" class="animate form">
        <section class="login_content">
          <form method="post">
            <h1>Create Account</h1>
			 <div>
              <input type="text" class="form-control" name="name" placeholder="Name" required="" />
            </div> 
			<div>
              <input type="email" class="form-control" name="email" placeholder="Email" required="" />
            </div>
			<div>
              <input type="text" class="form-control" name="phone_no" placeholder="Phone Number" required="" />
            </div>
			<div>
              <input type="text" class="form-control" name="address" placeholder="Address" required="" />
            </div>
			<?php if($_GET['type']=='lawyer'){?>
			<div>
              <input type="text" class="form-control" name="summary" placeholder="Profile Summary" required="" />
            </div>
			<?php }?>
            <div>
              <input type="text" class="form-control" name="username" placeholder="Username" required="" />
            </div>
            <div>
              <input type="password" class="form-control"  name="password" placeholder="Password" required="" />
            </div>
            <div>
              <input type="submit" name="signUp" class="btn btn-default submit" >Sign Up</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">
          <p class="change_link">New to site?
                <a href="#tologin" class="to_login"> Sign In</a>
              </p>
              <div class="clearfix"></div>
              <br />
              
              
              <div class="clearfix"></div>
              <br />
              <div>
                
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
    </div>
  </div>

</body>

</html>
