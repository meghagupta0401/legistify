-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2016 at 03:59 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `legistify`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_request`
--

CREATE TABLE IF NOT EXISTS `booking_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `lawyer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `accept_reject` int(11) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(25) NOT NULL,
  `statement` text NOT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `booking_request`
--

INSERT INTO `booking_request` (`request_id`, `lawyer_id`, `user_id`, `accept_reject`, `date`, `title`, `statement`) VALUES
(14, 7, 14, 1, '2016-04-14', 'Lost car need help', 'Please assist me ');

-- --------------------------------------------------------

--
-- Table structure for table `lawyer`
--

CREATE TABLE IF NOT EXISTS `lawyer` (
  `lawyer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `address` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `phone_no` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `profile_summary` text NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`lawyer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `lawyer`
--

INSERT INTO `lawyer` (`lawyer_id`, `name`, `address`, `email`, `phone_no`, `username`, `profile_summary`, `available`) VALUES
(7, 'D. Gopalani', 'Jaipur', 'gopu@gmail.com', 2147483647, 'gopalani', 'I specialize in Consumer cases and TOC. B.A. LLB', 1),
(8, 'Jackie Shroff', 'Mumbai', 'shroff@gmail.com', 2147483647, 'j.shroff', 'I specialize in Technology cases and ISS. B.A. LLB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`login_id`, `username`, `password`, `type`) VALUES
(13, 'himanshi', '123456789', 'user'),
(14, 'tim.martin', '123456789', 'user'),
(15, 'gopalani', '123456789', 'lawyer'),
(16, 'j.shroff', '123456789', 'lawyer');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` text NOT NULL,
  `sender` int(11) NOT NULL,
  `message` text NOT NULL,
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`user_id`, `date`, `title`, `sender`, `message`, `notification_id`, `request_id`) VALUES
(7, '2016-04-02 13:40:06', 'Lost car need help', 14, 'You have received a new application.', 10, 14);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `phone_no` int(11) NOT NULL,
  `email` varchar(20) NOT NULL,
  `address` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `phone_no`, `email`, `address`, `username`) VALUES
(14, 'Himanshi', 46433313, 'hmansh@gmail.com', 'Mumbai', 'himanshi'),
(15, 'Tim Martin', 2147483647, 'martin@gmail.com', 'Denmark', 'tim.martin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
