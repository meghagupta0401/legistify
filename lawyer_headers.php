<div class="col-md-3 left_col">
				<div class="left_col scroll-view">

					<div class="navbar nav_title" style="border: 0;">
						<a  class="site_title"><i class="fa fa-paw"></i> <span>LEGISTIFY!</span></a>
						
					</div>
					<div class="clearfix"></div>

					<!-- menu prile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="images/img.jpg" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Welcome,</span>
							<h2><?php echo $name;?></h2>
						</div>
					</div>
					<!-- /menu prile quick info -->

					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

						<div class="menu_section">
							<h3>General</h3>
							<ul class="nav side-menu">
								<li><a><i class="fa fa-home"></i> List Of Requests <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" style="display: none">
										<?php while($row=mysqli_fetch_array($requestslist))
{	
?>	
										<li>								       
										<form action="lawyerResponse.php" method="POST">
										<input type="hidden" name="request_id" value="<?php echo $row{'request_id'};?>	"></input>
										<input type=submit name="submit" value="<?php echo $row{'title'};?>" style="background-color:none"></input>
										</form>
										
										</li>
<?php } ?>
										
									</ul>
								</li>
								
							</ul>
						</div>
					</div>
					<!-- /sidebar menu -->

					
				</div>
			</div>

			 <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="images/img.jpg" alt=""><?php echo $name;?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
				<li>
				<?php 
				
				 $profile=mysqli_query($con,"select * from lawyer where lawyer_id='$_SESSION[lawyer_id]'");
$row=mysqli_fetch_array($profile);

	$available=$row['available'];
	

				if(!$available==0){ ?>
				<a href="status.php?status=0">
				<button  class="btn btn-success dropdown-toggle btn-sm" >Set status Unavailable
                  </button>
				  </a>
                 <?php } else{ ?>
				 <a href="status.php?status=1">
				<button  class="btn btn-danger dropdown-toggle btn-sm" href="status.php?status=true" >Set status Available
                  </button></a>
                 <?php } ?> 
                  </li>
				<li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                </ul>
              </li>

              <li role="presentation" class="dropdown">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-envelope-o"></i>
               
				<span class="badge bg-green"><?php
$notification=mysqli_query($con,"select * from notification where user_id='$_SESSION[lawyer_id]' ");
echo mysqli_num_rows($notification);
?> </span>
                </a>
				
			
		
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
				<?php
				
				
		while($row=mysqli_fetch_array($notification))
		{
	$sender=mysqli_query($con,"SELECT * FROM `user` WHERE `user_id`=$row[sender]");
	$sender_name=mysqli_fetch_array($sender);
			?>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                     
                                        <span><?php echo "<strong>".$sender_name['name']."</strong>-".$row{'title'};?></span>
                      <span class="time"><?php $row{'date'};?></span>
                     
					  <span class="message">
					  <form action="lawyerResponse.php" method="POST">
										<input type="hidden" name="request_id" value="<?php echo $row{'request_id'};?>	"></input>
										<input type=submit name="submit" value="<?php echo $row{'message'};?>"
										style="background-color:none"></input>
										</form>
                     </a>
                    </a>
                  </li>
		<?php } ?>
				  </ul>
              </li>

                </ul>
          </nav>
        </div>

      </div>
      